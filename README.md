## Installation

### If using yarn:

`yarn add react-components`

### If using npm:

`npm i react-components`

Usage
-----

### Initialization

#### Library Setup (in your React Native Project)

	cd <project base directory>
	yarn add git+https://bitbucket.org/atharvasystems/react-components.git or yarn add git+https://username@bitbucket.org/atharvasystems/react-components.git
	yarn install
	cd ios
	pod install
	cd ..